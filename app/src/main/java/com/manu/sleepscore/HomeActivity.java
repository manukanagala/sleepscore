package com.manu.sleepscore;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.manu.sleepscore.model.Currency;
import com.manu.sleepscore.model.ExchangeRates;
import com.manu.sleepscore.service.FixerApi;
import com.manu.sleepscore.service.FixerClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {


    private static final String TAG = HomeActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    BroadcastReceiver mMessageReceiver;
    CurrencyAdapter currencyAdapter;
    List<Currency> currencyList = new ArrayList<>();

    TextView baseTextView;
    TextView timeTextView;
    TextView counter;
    SharedPreferences spref;

    SensorService mSensorService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        spref = getSharedPreferences("androidchallenge", MODE_PRIVATE);

//        alarm = new AlarmManagerReceiver();
//        alarm.SetAlarm(this);
        mSensorService = new SensorService(this);
        Intent mServiceIntent = new Intent(this, mSensorService.getClass());

        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        counter = (TextView) findViewById(R.id.counter);
        baseTextView = (TextView) findViewById(R.id.base);
        timeTextView = (TextView) findViewById(R.id.time);

        currencyAdapter = new CurrencyAdapter(currencyList, R.layout.currency_item, getApplicationContext());
        recyclerView.setAdapter(currencyAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                System.out.println("Received");
                fetchCurrencyData();
            }
        };

        fetchCurrencyData();
    }

    private void updateInfo(String base) {

        counter.setText("" + spref.getInt("counter", 1));

        baseTextView.setText(base);

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd , yyyy, HH:mm a");

        timeTextView.setText(dateFormat.format(new Date()));

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction("com.api.fixer.RefreshAction");
        registerReceiver(mMessageReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }

    public void fetchCurrencyData() {
        FixerApi apiService =
                FixerClient.getClient().create(FixerApi.class);

        Call<ExchangeRates> call = apiService.getRates("USD");
        call.enqueue(new Callback<ExchangeRates>() {

            @Override
            public void onResponse(Call<ExchangeRates> call, Response<ExchangeRates> response) {
                System.out.println("****" + response.body());
                System.out.println("****" + response.toString());

                if (response.code() != 200)
                    return;
                Map<String, Double> exchangeRates = response.body().getRates();

                currencyList.clear();

                for (Map.Entry<String, Double> entry : exchangeRates.entrySet()) {
                    Currency currency = new Currency();
                    currency.setCode(entry.getKey());
                    currency.setRate(entry.getValue());
                    currencyList.add(currency);

                }
                currencyAdapter.notifyDataSetChanged();
                Log.d(TAG, "Number of currencies received: " + exchangeRates.size());
                updateInfo(response.body().getBase());

            }

            @Override
            public void onFailure(Call<ExchangeRates> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });

    }
}
