package com.manu.sleepscore.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manu on 2/21/2018.
 */

public class ExchangeRates
{
    private String base;
    private String date;
    private Map<String,Double> rates;

    public ExchangeRates(){
        rates = new HashMap<>();
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }
}
