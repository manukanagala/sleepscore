package com.manu.sleepscore.model;

/**
 * Created by Manu on 2/21/2018.
 */

public class Currency
{
    private String code;
    private double rate;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
