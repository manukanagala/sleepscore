package com.manu.sleepscore;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Manu on 2/21/2018.
 */

public class SensorService extends Service{

    public int counter=0;
    private static final String TAG = SensorService.class.getSimpleName();

    public SensorService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    SharedPreferences spref;

    public SensorService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        spref = getSharedPreferences("androidchallenge", MODE_PRIVATE);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ;
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 60*1000); //
    }


    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.api.fixer.RefreshAction");
                sendBroadcast(broadcastIntent);
                Log.i(TAG, "Sending broadcast intent");

                SharedPreferences.Editor editor = spref.edit();
                int currentCounter = spref.getInt("counter", 0);
                Log.i(TAG, "Current counter " + currentCounter);
                editor.putInt("counter", ++currentCounter);
                editor.commit();
            }

        };
    }


    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
