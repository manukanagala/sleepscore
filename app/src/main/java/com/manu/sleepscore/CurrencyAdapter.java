package com.manu.sleepscore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.manu.sleepscore.model.Currency;

import java.util.List;

/**
 * Created by Manu on 2/21/2018.
 */

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {

    private List<Currency> currencies;
    private int rowLayout;
    private Context context;

    @Override
    public CurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CurrencyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CurrencyViewHolder holder, int position) {
        holder.movieTitle.setText(currencies.get(position).getCode());
        holder.data.setText(""+currencies.get(position).getRate());


    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }


    public static class CurrencyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout moviesLayout;
        TextView movieTitle;
        TextView data;


        public CurrencyViewHolder(View v) {
            super(v);
            moviesLayout = (LinearLayout) v.findViewById(R.id.currency_layout);
            movieTitle = (TextView) v.findViewById(R.id.country);
            data = (TextView) v.findViewById(R.id.currency);

        }
    }

    public CurrencyAdapter(List<Currency> currencies, int rowLayout, Context context) {
        this.currencies = currencies;
        this.rowLayout = rowLayout;
        this.context = context;
    }



}
