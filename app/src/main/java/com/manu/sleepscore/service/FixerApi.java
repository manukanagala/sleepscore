package com.manu.sleepscore.service;

import com.manu.sleepscore.model.ExchangeRates;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Manu on 2/21/2018.
 */

public interface FixerApi
{

    @GET("latest")
    Call<ExchangeRates> getRates(@Query("base") String base) ;
}
